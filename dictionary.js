/*
 * Copyright 2017 Wproject - Aitzol Berasategi - https://aitzol.eus
 *
 * This file is part of Notes.
 *
 * Notes is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Notes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var dict = {
	en: {
		'theme':'theme',
		'language':'language',
		'cloud backup':'cloud backup',
		'about':'about',
		'cancel':'cancel',
		'accept':'accept',
		'save':'save',
		'title':'title',
		'add comments':'add comments',
		'delete':'delete',
		'Do you want to remove this note?':'Do you want to remove this note?',
		'storage':'storage',
		'Store data in the cloud':'Store data in the cloud',
		'Only if WiFi connection':'Only if WiFi connection',
		'Simple notes app by Wproject':'Simple notes app by Wproject',
		'License and Terms of use':'License and Terms of use',
		'Help translate':'Help translate',
		'Yes':'Yes',
		'No, thanks!':'No, thanks!',
		'OK':'OK',
		'Restore':'Restore',
		'Cloud Service':'Cloud Service',
		'Notes app could store your notes in the cloud. Would you like to try it?':'Notes app could store your notes in the cloud. Would you like to try it?',		
		'Unable to download from server! File not found! Maybe you could try adding new notes, this will create a new data file in your device!':'Unable to download from server! File not found! Maybe you could try adding new notes, this will create a new data file in your device!',
		'Data file not found! Would you like to try to restore data from cloud server?':'Data file not found! Would you like to try to restore data from cloud server?',
		'Connection error, please try again later!':'Connection error, please try again later!',
		'There was an error, please try again later!':'There was an error, please try again later!'
	},
	eu: {
		'theme':'gaia',
		'language':'hizkuntza',
		'cloud backup':'babeskopia',
		'about':'Notes-i buruz',
		'cancel':'utzi',
		'accept':'ados',
		'save':'gorde',
		'title':'izenburua',
		'add comments':'xehetasunak gehitu',
		'delete':'ezabatu',
		'Do you want to remove this note?':'Ohar hau ezabatu nahi al duzu?',
		'storage':'biltegiratzea',
		'Store data in the cloud':'Datuak lainoan gorde',
		'Only if WiFi connection':'WiFi konexioaz soilik',
		'Simple notes app by Wproject':'Wproject-ek garaturiko ohar applikazioa',
		'License and Terms of use':'Lizentzia eta erabilpen baldintzak',
		'Help translate':'Itzultzen lagundu',
		'Yes':'Bai',
		'No, thanks!':'Ez, eskerrik asko!',
		'OK':'ADOS',
		'Restore':'Berrezarri',
		'Cloud Service':'Laino Zerbitzua',
		'Notes app could store your notes in the cloud. Would you like to try it?':'Notes aplikazioak zure oharrak lainoan gorde ditzake. Probatu nahi al duzu?',
		'Unable to download from server! File not found! Maybe you could try adding new notes, this will create a new data file in your device!':'Ezinezkoa izan da zerbitzaritik deskarga burutzea! Ez da fitxategirik aurkitu! Agian ohar berriak gehitzen saia zintezke, horrek zure gailuan datu fitxategi berri bat sortuko du.',
		'Data file not found! Would you like to try to restore data from cloud server?':'Ez da datu fitxategirik aurkitu! Nahi al duzu laino zerbitzaritik zure datuak berreskuratzen saiatzea?',
		'Connection error, please try again later!':'Konexio akatsa, mesedez saia zaitez beranduago!',
		'There was an error, please try again later!':'Akats bat gertatu da, mesedez saia zaitez beranduago!'
	},
	es: {
		'theme':'tema',
		'language':'idioma',
		'cloud backup':'respaldo en nube',
		'about':'acerca de Notes',
		'cancel':'cancelar',
		'accept':'aceptar',
		'save':'guardar',
		'title':'titulo',
		'add comments':'añadir comentarios',
		'delete':'eliminar',
		'Do you want to remove this note?':'¿Quieres eliminar esta nota?',
		'storage':'almacenamiento',
		'Store data in the cloud':'Almacenar datos en la nube',
		'Only if WiFi connection':'Solo con conexion WiFi',
		'Simple notes app by Wproject':'Simple app de notas de Wproject',
		'License and Terms of use':'Licencia y condiciones de uso',
		'Help translate':'Ayudar a traducir',
		'Yes':'Si',
		'No, thanks!':'No, ¡gracias!',
		'OK':'ACEPTAR',
		'Restore':'Restaurar',
		'Cloud Service':'Sevicio en la Nube',
		'Notes app could store your notes in the cloud. Would you like to try it?':'La aplicación Notes puede almacenar tus notas en la nube. ¿Quieres probarlo?',	
		'Unable to download from server! File not found! Maybe you could try adding new notes, this will create a new data file in your device!':'¡No se ha podido realizar la descarga! ¡Fichero no encontrado! Quiza podrias intentar añadir nuevas notas, eso creara un fichero de datos en tu dispositivo.',
		'Data file not found! Would you like to try to restore data from cloud server?':'¡No se ha encontrado el fichero de datos! ¿Te gustaria tratar de restaurar tus datos desde el servidor en la nube?',
		'Connection error, please try again later!':'¡Error de conexion, por favor intentalo de nuevo más tarde!',
		'There was an error, please try again later!':'¡Se ha producido un error, por favor intentalo de nuevo más tarde!'
	}
};